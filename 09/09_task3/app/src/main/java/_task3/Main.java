package _task3;

public class Main {
    public static void main(String[] args) {
        Worker[] workers = new Worker[4];
        workers[0] = new Accountant("Валерия", 50000);
        workers[1] = new MainAccountant("Владислав", 80000);
        workers[2] = new Clerk("Иннокентий", 30000);
        workers[3] = new Engineer("Марк", 60000);

        for (int i = 0; i < workers.length; i++){
            System.out.println(workers[i] + workers[i].work());
            System.out.println(" ");
        }

    }
}
