package _task3;

public class Engineer extends Worker {

    public Engineer(String name, int salary) {
        super(name, salary, "Инженер");
    }

    @Override
    public String work(){
        return "Настраиваю сервер";
    }

    public String uniqueEngineer(){
        return "Уникальный метод инженера";
    }
}

