package _task3;

public class Accountant extends Worker {

    public Accountant(String name, int salary) {
        super(name, salary, "Бухгалтер");
    }


    @Override
    public String work(){
        return "Занимаюсь бухучетом";
    }

    public String uniqueAccountant(){
        return "Уникальный метод бухгалтера";
    }
}

