package _task3;

public class Clerk extends Worker{
    public Clerk(String name, int salary) {
        super(name, salary, "Обычный рабочий");
    }

    @Override
    public String work() {
        return "Просто работаю";
    }

    public String uniqueClerc(){
        return "ДЕЛО СДЕЛАНО";
    }
}
