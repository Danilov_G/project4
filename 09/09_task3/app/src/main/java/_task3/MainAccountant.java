package _task3;

public class MainAccountant extends Worker{
    public MainAccountant(String name, int salary) {
        super (name, salary, "Главный бухгалтер");

    }


    @Override
    public String work() {
        return "Организую работу бухгалтерии";
    }

    public String uniqueMainAccountant(){
        return "Уникальный метод главбухгалтера";
    }
}
