package _task3;


public abstract class Worker {
    private String name;
    private String pos;
    private int salary;

    public Worker(String name, int salary, String pos) {
        this.setName(name);
        this.setSalary(salary);
        this.setPosition(pos);
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPosition(String pos){
        this.pos = pos;
    }

    public String getPosition() {
        return pos;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public abstract String work();


    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    public int hashCode() {
        return (name.length() + pos.length() + salary) * 10;
    }

    public String toString() {
        return "Имя: " + name + "\n" + "Должность: " + pos + "\n" + "зп: " + salary + "\n";
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        Worker worker = (Worker) obj;
        return this.salary == worker.salary && this.pos == worker.pos && this.name == worker.name;
    }





}

