package _task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test void getterTest(){
        Vector a = new Vector(2,2);
        assertEquals(2 , a.getX());
    }

    @Test void toStringTest(){
        Vector a = new Vector(2,1);
        assertEquals("Координаты вектора (x: 2.0; y: 1.0)" , a.toString());
    }

    @Test void sumTest(){
        Vector a = new Vector(4,2);
        Vector b = new Vector(4,1);
        assertEquals(8 , Vector.sum(a,b).getX());
        assertEquals(3 , Vector.sum(a,b).getY());
    }

    @Test void subtractionTest(){
        Vector a = new Vector(4,3);
        Vector b = new Vector(8,4);
        assertEquals(-4 , Vector.sub(a,b).getX());
        assertEquals(-1 , Vector.sub(a,b).getY());
    }

    @Test void multiplyTest(){
        Vector a = new Vector(2,2);
        Vector.mul(a,2);
        assertEquals(4 , a.getX());
        assertEquals(4 , a.getY());
    }

    @Test void divisionTest(){
        Vector a = new Vector(2,2);
        Vector.div(a,2);
        assertEquals(1 , a.getX());
        assertEquals(1 , a.getY());
    }

}

