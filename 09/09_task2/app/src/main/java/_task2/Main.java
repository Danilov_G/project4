package _task2;

public class Main {
    public static void main(String[] args) {
        Vector a = new Vector(3, 4);
        Vector b = new Vector(2, 2);
        Vector c = Vector.sub(a, b);
        System.out.println(c);

        Vector d = Vector.sum(a, b);
        System.out.println(d);

        Vector e = Vector.mul(a, 3);
        System.out.println(e);

        Vector f = Vector.div(b, 2);
        System.out.println(f);
    }
}
