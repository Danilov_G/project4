package _task2;

public class Vector {

    private double x;
    private double y;

    public Vector(double x , double y){
        this.x = x;
        this.y = y;
    }

    public Vector(){}

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public String toString() {
        return "Координаты вектора (x: " + this.x + "; " + "y: "  + this.y + ")";
    }

    public static Vector sum(Vector v1, Vector v2){
        Vector res = new Vector();
        res.x = v1.x + v2.x;
        res.y = v1.y + v2.y;
        return res;
    }

    public static Vector sub(Vector v1, Vector v2){
        Vector res = new Vector();
        res.x = v1.x - v2.x;
        res.y = v1.y - v2.y;
        return res;
    }

    public static Vector mul(Vector v1, double a){
        v1.x *= a;
        v1.y *= a;
        return v1;
    }

    public static Vector div(Vector v1, double a){
        v1.x /= a;
        v1.y /= a;
        return v1;
    }
}


