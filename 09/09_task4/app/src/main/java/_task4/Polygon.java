package _task4;

public class Polygon implements MyInterface{
    protected int perimeter;
    protected int[] sides;

    Polygon(int[] sides){
        this.sides = sides;
        this.perimeter = polygonPerimeter();
        this.checkPolygonCorrect();
    }

    Polygon(){}

    public int polygonPerimeter(){
        int perimeter = 0;
        for (int i = 0; i < this.sides.length; i++) {
            perimeter += this.sides[i];
        }
        return perimeter;
    }

    public boolean checkPolygonCorrect(){
        int sum = this.perimeter;
        for (int i = 0; i < this.sides.length; i++){
            if (2 * this.sides[i] > sum){
                return false;
            }
        }
        return true;
    }

    public String toString(){
        return "Это " + this.sides.length + "-угольник. Периметр: " + this.perimeter;
    }

    public void output(){
        System.out.println(this);
    }
}

