package _task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {

    @Test
    void polygonPerimeterTest(){
        int[] a = new int[]{1,2,3};
        Polygon b = new Polygon(a);
        assertEquals(6,b.polygonPerimeter());
    }

    @Test void toStringPolygon(){
        int[] a = new int[]{1,1,1,1,1};
        Polygon b = new Polygon(a);
        assertEquals("Это 5-угольник. Периметр: 5", b.toString());
        assertEquals("Я фигура", b.toString());
    }

    @Test void checkTrueCorrect(){
        int[] a = new int[]{4,24,100};
        Polygon b = new Polygon(a);
        assertFalse(b.checkPolygonCorrect());
    }

    @Test void checkFalseCorrect(){
        int[] a = new int[]{1,2,3};
        Polygon b = new Polygon(a);
        assertTrue(b.checkPolygonCorrect());
    }
}

