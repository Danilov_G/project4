package _task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest{

    @Test void trianglePerimeterTest() throws NotPolygonException{
        int[] a = new int[]{1,1,1};
        Triangle b = new Triangle(a);
        assertEquals(3, b.polygonPerimeter());
    }

    @Test void toStringTriangle() throws NotPolygonException{
        int[] a = new int[]{1,1,1};
        Triangle b = new Triangle(a);
        assertEquals("Это треугольник. Периметр: 3", b.toString());
    }

    @Test void ExceptionTest() throws NotPolygonException{
        int[] a = new int[]{1,1};
        NotPolygonException exception = assertThrows(NotPolygonException.class, () -> new Triangle(a));
        assertEquals("Это не треугольник", exception.message);
    }
}

