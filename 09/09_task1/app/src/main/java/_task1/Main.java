package _task1;


public class Main {
    public static void main(String[] args) throws MatrixException {

        Actions action = new Actions();
        int rows1 = 2;
        int columns1 = 4;
        Matrix mtrx1 = new Matrix(rows1, columns1);
        for (int i = 0; i < mtrx1.getRows(); i++) {
            for (int j = 0; j < mtrx1.getColumns(); j++) {
                System.out.print(mtrx1.getMatrix()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int rows2 = 2;
        int columns2 = 4;
        Matrix mtrx2 = new Matrix(rows2, columns2);
        action.printMatrix(mtrx2);

        System.out.println("Summa: ");
        int[][] sumRes = Actions.sum(mtrx1, mtrx2);
        action.printMatrix(sumRes);

        System.out.println("Raznost: ");
        int[][] subRes = Actions.sub(mtrx1, mtrx2);
        action.printMatrix(subRes);

        int rows3 = 4;
        int columns3 = 2;
        Matrix mtrx3 = new Matrix(rows3, columns3);
        action.printMatrix(mtrx3);

        System.out.println("Ymnozenie: ");
        int[][] mulRes = Actions.mul(mtrx1, mtrx3);
        action.printMatrix(mulRes);
    }
}

