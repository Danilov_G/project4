package _task1;

public class Actions {

    public static int[][] sub(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
        int[][] result;
        int[][] matrix1 = m1.getMatrix();
        int[][] matrix2 = m2.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new  MatrixException("Матрицы не одинакового размера");
        }
        result = new int[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                result[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        }
        return result;
    }

    public static int[][] sum(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
        int[][] result;
        int[][] matrix1 = m1.getMatrix();
        int[][] matrix2 = m2.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new  MatrixException("Матрицы не одинакового размера");
        }
        result = new int[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }

    public static int[][] mul(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
        int[][] result;
        int[][] matrix1 = m1.getMatrix();
        int[][] matrix2 = m2.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (matrix1[0].length != matrix2.length) {
            throw new  MatrixException("Нельзя умножать такие матрицы");
        }
        result = new int[matrix1.length][matrix2[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) { 
                for (int k = 0; k < matrix2.length; k++) {
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return result;
    }
    public void printMatrix(int[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printMatrix(Matrix m1){
        int[][] matrix = m1.getMatrix();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}






