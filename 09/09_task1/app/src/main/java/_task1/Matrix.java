package _task1;


public class Matrix {
    private int[][] matrix;
    private int rows;
    private int columns;

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public void setRows(int rows) {
        this.rows = rows;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public void setColumns(int columns) {
        this.columns = columns;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void generateMatrix() {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                this.matrix[i][j] = (int) (Math.random() * 10);
            }
        }
    }

    public void setMatrix(int[][] matrix) throws MatrixException {
        if (matrix.length == this.rows) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length != this.columns) {
                    throw new MatrixException("Матрица задана неверно");
                }
            }
        }
        else {
            throw new MatrixException("Матрица задана неверно");
        }

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }
}

