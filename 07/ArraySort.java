public class ArraySort {
    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 3, 5, 13, 2, 35, 1, 3, 7};
        int n = arr.length;
        int tmp = 0;
        int i = 0;
        while(i < n - 1){
                if (arr[i] > arr[i + 1]){
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    i = 0;
                } else {
                    i++;
                }
            }
        for (int k = 0; k < n; k++) {
            System.out.print(arr[k] + " ");
        }
    }
}
