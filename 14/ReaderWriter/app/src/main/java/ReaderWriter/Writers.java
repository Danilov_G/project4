package ReaderWriter;

import java.util.Random;
public class Writers implements Runnable {

    private WStatus status = WStatus.WAIT_STATUS;
    private Book book;
    public Writers(Book book) {
        this.book = book;
    }

    Random random = new Random();

    public void run() {
        status = WStatus.WRITE_STATUS;
        book.write((char)(random.nextInt(26) + 'a'));
        status = WStatus.WAIT_STATUS;

    }
}


