package ReaderWriter;
public class Readers implements Runnable {

    private RStatus status = RStatus.WAIT_STATUS;
    private Book book;
    public Readers(Book book) {
        this.book = book;
    }
    public void run() {
        status = RStatus.READ_STATUS;
        book.read();
        status = RStatus.WAIT_STATUS;
    }
}


