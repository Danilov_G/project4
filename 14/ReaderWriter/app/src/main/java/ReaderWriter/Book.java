package ReaderWriter;

import java.util.ArrayList;
import java.util.stream.Collectors;
public class Book {
    private static final int MIN_TIME_FOR_READ = 200;
    private static final int MAX_TIME_FOR_READ = 700;
    private static final int MIN_TIME_FOR_WRITE = 500;
    private static final int MAX_TIME_FOR_WRITE = 1500;
    private boolean nowWrite = false;
    private ArrayList<Character> resource = new ArrayList<>();
    public String read() {
        while (nowWrite) {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {}
            }
        }
        String result = resource.stream()
                .map(String::valueOf)
                .collect(Collectors.joining());
        System.out.format("Из книги %s достал данные: %s\n", Thread.currentThread().getName(), result);
        try {
            Thread.sleep((MIN_TIME_FOR_READ + (int) (Math.random() * (MAX_TIME_FOR_READ - MIN_TIME_FOR_READ))) * result.length());
        } catch (InterruptedException e) {}
        return result;
    }
    public synchronized void write(char data) {
        nowWrite = true;
        System.out.format("В книгу %s ставит данные: %s\n", Thread.currentThread().getName(), data);
        try {
            Thread.sleep((MIN_TIME_FOR_WRITE + (int) (Math.random() * (MAX_TIME_FOR_WRITE - MIN_TIME_FOR_WRITE))));
        } catch (InterruptedException e) {}
        resource.add(data);
        System.out.format("В книгу %s написал текст: %s\n", Thread.currentThread().getName(), data);
        nowWrite = false;
        notifyAll();
    }
}


