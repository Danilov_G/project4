package ReaderWriter;
public class Main {
    private static final int MIN_TIME_FOR_WAITING = 1000;
    private static final int TIME_FOR_WAITING_INTERVAL = 1000;
    public static void main(String[] args) {
        Book resource = new Book();
        while (true) {
            sleep();
            new Thread(new Writers(resource)).start();
            sleep();
            new Thread(new Readers(resource)).start();
        }
    }
    private static void sleep() {
        try {
            Thread.sleep(MIN_TIME_FOR_WAITING + (int)(Math.random() * TIME_FOR_WAITING_INTERVAL));
        } catch (InterruptedException e) {}
    }
}

