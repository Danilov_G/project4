package DiningPhilosophers;

import java.util.concurrent.Semaphore;
public class Fork {
    Semaphore accessToFork = new Semaphore(1, true);
    public boolean forkIsBusy(){
        return this.accessToFork.availablePermits() > 0;
    }
    public void takeFork() {
        try {
            this.accessToFork.acquire();
        } catch (InterruptedException e) {}
    }
    public void putFork() {
        this.accessToFork.release();
    }
}

