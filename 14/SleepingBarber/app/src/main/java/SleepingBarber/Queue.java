package SleepingBarber;

import java.util.concurrent.ArrayBlockingQueue;

public class Queue {
    ArrayBlockingQueue<Customer> сlientsQueue;
    private boolean corridorIsClosed = false;
    Queue(int numberOfChairs) {
        сlientsQueue = new ArrayBlockingQueue<>(numberOfChairs);
    }
    public synchronized int getNumberOfClients() {
        return сlientsQueue.size();
    }
    public synchronized boolean sitOnAChair(Customer customer) {
        return сlientsQueue.offer(customer);
    }
    public synchronized Customer getClient() {
        return сlientsQueue.poll();
    }
    public synchronized void closeCorridor() {
        while (corridorIsClosed) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        corridorIsClosed = true;
    }
    public synchronized void openCorridor() {
        corridorIsClosed = false;
        notifyAll();
    }
}

