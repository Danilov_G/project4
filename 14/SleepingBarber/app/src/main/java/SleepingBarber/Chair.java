package SleepingBarber;

public class Chair {
    private boolean chairIsBusy = false;
    private synchronized void occupy(String nameOfSitting) {
        while (chairIsBusy) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        System.out.println(nameOfSitting + " сидит в кресле");
        chairIsBusy = true;
    }
    public void occupy(Customer client) {
        occupy(client.getName());
    }
    public void occupy() {
        occupy(Thread.currentThread().getName());
    }
    public synchronized void vacate() {
        System.out.println("Кресло освободилось");
        chairIsBusy = false;
        notifyAll();
    }
}

