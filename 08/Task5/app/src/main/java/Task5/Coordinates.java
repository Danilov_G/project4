package Task5;

import java.util.Random;
import java.util.Scanner;

public class Coordinates {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите кол-во повторений: ");
        int repeat = scan.nextInt();
        System.out.print("Введите размер области: ");
        int size = scan.nextInt();
        System.out.print("Сдвиг: ");
        int swipe = scan.nextInt();
        int count = 0;
        for(int i = 0; i < repeat; i++){
            int a = new Random().nextInt(size) + swipe;
            int b = new Random().nextInt(size) + swipe;
            int c = new Random().nextInt(size) + swipe;
            int d = new Random().nextInt(size) + swipe;
            if ((c > a && b > c) || (d > a && b > d) || (a > c && d > a) || (b > c && d > b)) || (a > d && d > c){
                count++;
            }
        }
        System.out.println("Кол-во пересечений: " +count);
    }

}
