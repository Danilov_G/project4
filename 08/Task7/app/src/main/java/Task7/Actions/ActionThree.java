package Task7.Actions;

public class ActionThree extends Action{
    public ActionThree(String str){
        super(str);
    }
    public void act(){
        System.out.println("Думайте о приятном. Сегодня многое будет зависеть от вашего настроя, и лучше сосредоточиться на хорошем. ");
    }
}

