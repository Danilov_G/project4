package Task7.Actions;

public class ActionFour extends Action{
    public ActionFour(String str){
        super(str);
    }
    public void act(){
        int rnd = (int)((Math.random() * 3) + 1);
        if (rnd == 1){
            System.out.println("Ричард III\n У каждого зверя есть чувство жалости... У меня нет, потому я не зверь...");
        }
        if (rnd == 2){
            System.out.println("Ромео и Джульетта\n Так сладок мёд, что, наконец, он горек.\n Избыток вкуса убивает вкус.");
        }
        if (rnd == 3){
            System.out.println("Гамлет\n Советы принимай от всех дающих, но собственное мнение береги.");
        }
    }
}
