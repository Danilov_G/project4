package Task7;


import Task7.Actions.*;
import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int userResponse = 0;
        LuckTest begin = new LuckTest();
        LuckTest level1 = new LuckTest();
        LuckTest level1_2 = new LuckTest();
        LuckTest level2 = new LuckTest();
        LuckTest level2_2 = new LuckTest();
        Action one = new ActionOne("Определение квантовой физики");
        begin.connection(level1);
        begin.connection(level1_2);
        begin.setAction(one);
        level1.setAction(new ActionTwo("Русская рулетка"));
        level1.connection(level2);
        level1.connection(level2_2);
        level1_2.setAction(new ActionThree("Получить совет"));
        level2.setAction(new ActionFour("Цитаты из произведений Шекспира"));
        level2_2.setAction(new ActionFive("СЕКРЕТНЫЙ МЕТОД"));

        while(true){
            begin.printStr();
            userResponse = in.nextInt();
            System.out.println();

            if (userResponse == 0){
                if (begin.parent == null){
                    break;
                }
                else {
                    begin = begin.parent;
                }
            }

            if(userResponse > begin.children.size() && userResponse <= begin.actions.size() + begin.children.size()){
                begin.actions.get(userResponse - begin.children.size() - 1).act();
            }
            if(userResponse > 0 && userResponse <= begin.children.size()){
                begin = begin.children.get(userResponse - 1);
            }
        }
    }
}
