package Task7;

import java.util.ArrayList;
import Task7.Actions.Action;

public class LuckTest {
    ArrayList<LuckTest> children = new ArrayList<LuckTest>();
    LuckTest parent;
    ArrayList<Action> actions = new ArrayList<Action>();

    public void setAction(Action action){
        this.actions.add(action);
    }

    public void connection(LuckTest nextLevel){
        this.children.add(nextLevel);
        nextLevel.parent = this;
    }

    public void printStr(){
        System.out.println("Действия с меню:");
        if (this.parent == null){
            System.out.println("0 Выйти");
        }
        else{
            System.out.println("0 Вернуться на уровень назад");
        }
        int num = 1;
        for (int i = 0; i < this.children.size(); i++) {
            System.out.println(num + " " + "Перейти на следующий уровень");
            num++;
        }
        for (int i = 0; i < this.actions.size(); i++) {
            System.out.println(num + " " + this.actions.get(i).getStr());
            num++;
        }
        System.out.println();
    }
}


