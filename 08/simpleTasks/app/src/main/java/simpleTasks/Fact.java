
package simpleTasks;

import java.math.BigInteger;

public class Fact {
    public static BigInteger rec(int a) {
        if (a <= 1) {
            return BigInteger.valueOf(1);
        } else {
            return BigInteger.valueOf(a).multiply(rec(a - 1));
        }
    }

    public static BigInteger fact(int b) {
        BigInteger res = BigInteger.ONE;
        for (int i = 1; i <= b; i++)
            res = res.multiply(BigInteger.valueOf(i));
        return res;
    }

    public static void main(String[] args) {
        System.out.println(rec(10));
        System.out.println(fact(30));
    }


}
