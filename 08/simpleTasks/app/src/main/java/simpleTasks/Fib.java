package simpleTasks;

import java.util.*;
import java.lang.*;

public class Fib {
    public static long recFib(int a, long[] fibbers){
        if (fibbers[a - 1] != 0){
            return fibbers[a - 1];
        }
        if (a <= 2){
            fibbers[a - 1] = a - 1;
            return fibbers[a - 1];
        }
        recFib(a - 1, fibbers);
        fibbers[a - 1] = fibbers[a - 2] + fibbers[a - 3];
        return fibbers[a - 1];
    }

    public static long[] getFib(int n){
        if (n == 0){
            return null;
        }
        long[] fibbers_new = new long[n];
        recFib(n, fibbers_new);
        return fibbers_new;
    }

    public static long[] fib(int a) {
        long first = 0;
        long second = 1;
        long end = a;
        long[] arr = new long[a];
        if (end == 1) {
            arr[0] = first;
        } else if (end > 1) {
            arr[0] = first;
            arr[1] = second;
        }

        for (int i = 0; i < end - 2; i++) {
            long res = first + second;
            arr[i+2] = res;
            first = second;
            second = res;
        }

        return arr;
    }

}
