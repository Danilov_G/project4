
package simpleTasks;

public class CharInString {
    public static int func(String line, char symbol) {
        char[] arr = line.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (symbol == arr[i]) {
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args){
        System.out.println(func("aaaaaaGhJIJMlL1337", '1') + " повторений");
    }
}
