package simpleTasks;

public class DiagMatrix {
    public static void main(String[] args){
        int[][] arr = {{1,2,3}, {4,5,6}, {7,8,9}};
        System.out.println("Матрица: ");
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr.length; j++){
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Ниже главной диагонали");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i > j) {
                    System.out.print(arr[i][j] + " ");
                }
            }
            System.out.println();
        }

    }
}

