package simpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibTest {
    @Test void testRecFib() {
        int a = 10;
        long[] b = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        assertArrayEquals(b, Fib.getFib(a));
    }
    @Test void testFib() {
        int a = 10;
        long[] b = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        assertArrayEquals(b, Fib.fib(a));
    }
}

