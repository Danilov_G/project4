package simpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CharTest {
    @Test void charInString(){
        assertEquals(5, CharInString.func("111AHjkc1iwq1p", '1'));
    }
}
