package simpleTasks;

import org.junit.jupiter.api.Test;
import java.math.BigInteger;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactTest {
    @Test void fact(){
        BigInteger n1 = BigInteger.valueOf(3628800);
        BigInteger n2 = Fact.fact(10);
        assertEquals(n1, n2);
    }
    @Test void recFact(){
        BigInteger n1 = BigInteger.valueOf(3628800);
        BigInteger nRec = Fact.rec(10);
        assertEquals(n1, nRec);
    }

}
