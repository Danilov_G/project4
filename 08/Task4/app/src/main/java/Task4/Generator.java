package Task4;

import java.util.Scanner;

public class Generator {
    public static void main(String[] args) throws InterruptedException {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите коэффициент: ");
        double coef = scan.nextDouble();
        System.out.print("Введите порог: ");
        double treshold = scan.nextDouble();
        System.out.print("Введите время: ");
        int max_time = scan.nextInt();



        genProc(coef, treshold, max_time);
    }

    public static void genProc(double coef, double treshold, int max_time ) throws InterruptedException {
        double[] arr = new double[2147483647];
        long sTime = System.currentTimeMillis();
        double min = -treshold * coef;
        double max = treshold * coef;

        int j = 0;
        do {
            Thread.sleep((long) (500 + Math.random() * 1000));
            arr[j] = (Math.random() * ((max - min) + 1)) + min;
            System.out.println((System.currentTimeMillis() - sTime)/1000 + ": " + arr[j] + " ");
            j++;
        } while (arr[j] < treshold && max_time > (System.currentTimeMillis() - sTime)/1000);
    }

}

