package Task1;

import java.util.Scanner;

public class StringToInt {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String a = scan.nextLine();

        try {
            Integer i = Integer.valueOf(a);
            System.out.println(i);
        } catch (NumberFormatException e) {
            System.err.println("Строка не является числом ИЛИ число слишком большое");
        }
    }
}
