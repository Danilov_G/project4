package Task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TimeGenTest {
    @Test void generatorTimeError() {
        TimeGen TimeGen = new TimeGen(10, 10000000);
        TimeGen.generate();
        assertFalse(TimeGen.success());
    }

    @Test void generatorSuccess() {
        TimeGen TimeGen = new TimeGen(1000, 100);
        TimeGen.generate();
        assertTrue(TimeGen.success());
    }

    @Test void generatorArrayNotEmpty() {
        TimeGen TimeGen = new TimeGen(1000, 100);
        TimeGen.generate();
        assertNotNull(TimeGen.getArr());
    }

    @Test void generatorTimeErrorArray() {
        TimeGen TimeGen = new TimeGen(10, 10000000);
        TimeGen.generate();
        assertNull(TimeGen.getArr());
    }

}
