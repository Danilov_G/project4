package Task2;

public class TimeGen {
    double timeout;
    double[] arr;
    int n;
    double spentTime;

    TimeGen(double timeout, int n) {
        this.timeout = timeout;
        this.n = n;
    }

    boolean success() {
        return spentTime < timeout;
    }

    public double[] getArr() {
        if (success()) {
            return this.arr;
        }
        else {
            return null;
        }
    }

    void generate() {
        arr = new double[n];
        double time = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            arr[i] = Math.random();
        }
        this.spentTime = System.currentTimeMillis() - time;
        System.out.println("Время генерации = " + spentTime + " секунд");
    }

    void showMassive(){
        if(success()){
            for(int i = 0; i < arr.length; i++){
                System.out.println(arr[i]);
            }
        }
    }

}
