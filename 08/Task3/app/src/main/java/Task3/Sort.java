package Task3;

public class Sort {
    public static void sort(double[] arr) {
        int n = arr.length;
        double tmp = 0;
        int i = 0;
        while (i < n - 1) {
            if (arr[i] > arr[i + 1]) {
                tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                i = 0;
            } else {
                i++;
            }
        }
    }

    public static double[] createSortedArray(double[] arr) {
        double[] arr2 = arr.clone();
        sort(arr2);
        return arr2;
    }
}
