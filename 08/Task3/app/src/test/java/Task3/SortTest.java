package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SortTest {
    @Test void sortTest(){
        double[] arr = {1.0, 2.0, 8.0, 4.0, 5.0};
        Sort.sort(arr);
        double[] sortArr = {1.0, 2.0, 4.0, 5.0, 8.0};
        assertArrayEquals(sortArr, arr);
    }
    @Test void createSortArr(){
        double[] arr = {1.0, 2.0, 8.0, 4.0, 5.0};
        double[] sortArr = {1.0, 2.0, 4.0, 5.0, 8.0};
        assertArrayEquals(sortArr, Sort.createSortedArray(arr));
    }
}
