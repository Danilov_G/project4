package Task6;

import java.util.Scanner;

class Matrix {
    public static int[][][] generate(int num, int size) {
        int[][][] matrix = new int[num][size][size];
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    matrix[i][j][k] = (int)(Math.random() * 10);
                }
            }
        }
        return matrix;
    }


    public static int[][] multiply(int[][] arr1, int[][] arr2) {
        int[][] res = new int[arr1.length][arr2[0].length];
        for (int i = 0; i < arr1.length; i++) {
                for (int j = 0; j < arr2[0].length; j++) {
                    for (int k = 0; k < arr2.length; k++) {
                        res[i][j] += arr1[i][k] * arr2[k][j];
                    }
                }
            }
            return res;
        }



    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите количество матриц: ");
        int num = scan.nextInt();
        System.out.print("Введите размер матриц: ");
        int size = scan.nextInt();

        long timeStart = System.currentTimeMillis();
        int[][][] arrOfMatrix = generate(num, size);
        for (int i = 0; i < num - 1; i++) {
            arrOfMatrix[i + 1] = multiply(arrOfMatrix[i], arrOfMatrix[i + 1]);
        }

        System.out.println("Время выполнения: " + (System.currentTimeMillis() - timeStart) + " мс");
    }
}

