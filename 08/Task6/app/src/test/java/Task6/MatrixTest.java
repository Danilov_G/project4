package Task6;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class MatrixTest {

    @Test void MultiplyMatrixes() {
        int[][] matrixA = {{1,2}, {3,4},{5,6}};
        int[][] matrixB = {{5,4,3}, {2,1,0}};
        int[][] matrixC = {{9,6,3},{23,16,9},{37,26,15}};
        assertArrayEquals(matrixC, Matrix.multiply(matrixA, matrixB));
    }

    @Test void MultiplyNullMatrixes() {
        int[][] matrixA = {{0}};
        int[][] matrixB = {{0}};
        int[][] matrixC = {{0}};
        assertArrayEquals(matrixC, Matrix.multiply(matrixA, matrixB));
    }

}
