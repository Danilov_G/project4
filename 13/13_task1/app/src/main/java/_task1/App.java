package _task1;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public boolean checkCorrectPhone(String phone){
        String regex = "^(8|\\+7)[-]?(([(]\\d{3}[)])|\\d{3})[-]?\\d{3}[-]?\\d{2}[-]?\\d{2}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }


}

