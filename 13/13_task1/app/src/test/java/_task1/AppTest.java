package _task1;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void Test1(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("+7(924)447-24-88"));
    }
    @Test void Test2(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("+79244472488"));
    }
    @Test void Test3(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("+7-(924)-447-24-88"));
    }
    @Test void Test4(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("89244472488"));
    }
    @Test void Test5(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("8(918)6036391"));
    }
    @Test void Test6(){
        App app = new App();
        assertTrue(app.checkCorrectPhone("8-(918)-603-63-91"));
    }
    @Test void Test7(){
        App app = new App();
        assertFalse(app.checkCorrectPhone("123456789012"));
    }
    @Test void Test8(){
        App app = new App();
        assertFalse(app.checkCorrectPhone("+7924)447-24-88"));
    }
    @Test void Test9(){
        App app = new App();
        assertFalse(app.checkCorrectPhone("+7(924447-24-88"));
    }
}
