package Structure;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SinglyLinkedListTest {
    @Test void addTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(1);
        assertTrue(list.add(a));
    }

    @Test void sizeTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(1);
        list.add(a);
        assertEquals(1,list.size());
    }

    @Test void sizeEmptyListTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        assertEquals(0,list.size());
    }

    @Test void isNotEmptyTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        list.add(a);
        assertFalse(list.isEmpty());
    }

    @Test void isEmptyTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        assertTrue(list.isEmpty());
    }

    @Test void trueContainsTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        list.add(a);
        assertTrue(list.contains(a));
    }

    @Test void falseContainsTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        Node<Integer> b = new Node<Integer>(1);
        list.add(a);
        assertFalse(list.contains(b));
    }

    @Test void removeTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        Node<Integer> b = new Node<Integer>(1);
        list.add(a);
        list.add(b);
        list.remove(b);
        assertNull(list.head.getNext());
    }

    @Test void clearTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        Node<Integer> b = new Node<Integer>(1);
        list.clear();
        assertNull(list.head);
    }

    @Test void getTest(){
        SinglyLinkedList list = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(0);
        Node<Integer> b = new Node<Integer>(1);
        list.add(a);
        list.add(b);
        assertSame(list.head, list.get(0));
    }

    @Test void equalsTest(){
        SinglyLinkedList first = new SinglyLinkedList();
        SinglyLinkedList second = new SinglyLinkedList();
        Node<Integer> a = new Node<Integer>(1);
        first.add(a);
        second.add(a);
        assertTrue(first.equals(second));
    }
}

