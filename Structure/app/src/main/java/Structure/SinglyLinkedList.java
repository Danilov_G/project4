package Structure;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SinglyLinkedList<T> implements List<T> {

    Node<T> head = null;
    Node<T> tail = null;

    public int size() {
        Node<T> node = this.head;
        int size = 0;
        while (node != null) {
            size++;
            node = node.getNext();
        }
        return size;
    }

    public boolean isEmpty() {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean contains(Object o) {
        Node<T> node = this.head;
        while (node != null) {
            if (node.getData() == o) {
                return true;
            }
            node = node.getNext();
        }
        return false;
    }

    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> current = head;

            public boolean hasNext() {
                if (current != null) {
                    return true;
                }
                return false;
            }

            public T next() {
                T data = (T) current.getData();
                current = current.getNext();
                return data;
            }
        };
    }

    public T[] toArray() {
        int size = this.size();
        T[] arr = (T[]) new Object[size];
        Node<T> node = this.head;
        for (int i = 0; i < size; i++) {
            arr[i] = (T) node.getData();
            node = node.getNext();
        }
        return arr;
    }

    public boolean add(T data) {
        Node<T> node = new Node<T>((T) data);
        if (this.head == null) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.setNext(node);
            this.tail = node;
        }
        return true;
    }

    public boolean remove(Object o) {
        Node<T> node = this.head;
        Node<T> next = new Node<T>();
        while (node != null) {
            if (node.getNext().getData() == o) {
                next = node.getNext();
                node.setNext(next.getNext());
                next.setData(null);
                next.setNext(null);
                return true;
            }
        }
        return false;
    }


    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }


    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        Node<T> next = new Node<T>();
        while (this.head != null) {
            next = this.head.getNext();
            this.head.setData(null);
            this.head.setNext(null);
            this.head = next;
        }
    }


    public T get(int index) {
        if(index < 0 || index > this.size()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return (T) this.head;
        }
        Node<T> node = this.head;
        while (index != 0) {
            index--;
            node = node.getNext();
        }
        return (T) node;
    }


    public T set(int index, Object element) {
        throw new UnsupportedOperationException();
    }


    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }


    public T remove(int index) {
        throw new UnsupportedOperationException();
    }


    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }


    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }


    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }


    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }


    public List subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }


    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }


    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }


    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] a) {
        int size = this.size();
        a = new Object[size];
        Node<T> node = this.head;
        for (int i = 0; i < size; i++) {
            a[i] = node.getData();
            node = node.getNext();
        }
        return a;
    }

    public boolean equals(Object o) {
        if (!(o instanceof SinglyLinkedList)) {
            System.out.println("Other class");
            return false;
        }
        SinglyLinkedList<T> newList = (SinglyLinkedList<T>) o;
        if (this.size() != newList.size()) {
            System.out.println("Other size");
            return false;
        }
        Node<T> node = this.head;
        Node<T> newNode = newList.head;
        while (node != null) {
            if (node.getData() != newNode.getData()) {
                return false;
            }
            node = node.getNext();
            newNode = newNode.getNext();
        }
        return true;
    }
}

