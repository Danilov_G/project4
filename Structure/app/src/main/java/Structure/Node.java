package Structure;

public class Node <T> {

    private Node next = null;
    private T data = null;

    public Node(){
        this.next = null;
        this.data = null;
    }

    public Node(T data){
        this.next = null;
        this.data = data;
    }

    public T getData(){
        return this.data;
    }

    public Node getNext(){
        return this.next;
    }

    public void setData(T newData){
        this.data = newData;
    }

    public void setNext(Node newNext){
        this.next = newNext;
    }


}

