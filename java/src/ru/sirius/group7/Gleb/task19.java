package ru.sirius.group7.Gleb;

public class task19 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 6, 3, 100, 0, 5};
        int min = 0;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < min){
                min = arr[i];
            }
        }
        System.out.println("Минимальный элемент массива: " + min);

        for (int j = 0; j < arr.length; j++) {
            if(arr[j] - min > 5){
                count++;
            }
        }
        System.out.println("Колво элементов, больше минимального на 5: " + count);
    }
}
