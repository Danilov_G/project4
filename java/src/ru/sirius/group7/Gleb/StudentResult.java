package ru.sirius.group7.Gleb;

public class StudentResult {
    public String name;
    public double result;

    public StudentResult(String name, double result) {
        this.name = name;
        this.result = result;
    }
}
