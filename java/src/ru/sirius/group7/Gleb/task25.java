package ru.sirius.group7.Gleb;

public class task25 {
    public static void main(String[] args) {
        int count = 0;
        RNG[] arr = new RNG[10];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = new RNG();
            if(Math.abs(arr[i].a - arr[i].b) <= 5){
                count++;
            }
        }

        System.out.println(count);
    }
}
