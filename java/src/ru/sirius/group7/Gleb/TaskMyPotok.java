package ru.sirius.group7.Gleb;

public class TaskMyPotok {
    public static void main(String[] args) throws InterruptedException {
        MyPotok[] myPotok = new MyPotok[10];
        myPotok[0] = new MyPotok();
        myPotok[1] = new MyPotok();
        myPotok[2] = new MyPotok();
        myPotok[3] = new MyPotok();
        myPotok[4] = new MyPotok();
        myPotok[5] = new MyPotok();
        myPotok[6] = new MyPotok();
        myPotok[7] = new MyPotok();
        myPotok[8] = new MyPotok();
        myPotok[9] = new MyPotok();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < myPotok.length; i++) {
            Thread.sleep((long)(Math.random() * 10000) + 5000);
            myPotok[i].start();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Время выполнения команды: " + ((endTime-startTime) / 1000) + "сек");
    }
}
