package ru.sirius.group7.Gleb;

public class Bird extends Mob implements Fly{
    public Bird(int hp) {
        super(hp);
    }

    @Override
    public void move() {
        System.out.println("This bird is moving");
    }

    public void fly() {
        System.out.println("This bird is flying");
    }
}
