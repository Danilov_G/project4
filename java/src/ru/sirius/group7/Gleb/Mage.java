package ru.sirius.group7.Gleb;

public class Mage extends Mob implements Fly{
    public Mage(int hp) {
        super(hp);
    }

    @Override
    public void move() {
        System.out.println("This mage is moving");
    }

    public void fly() {
        System.out.println("This mage is flying");
    }
}
