package ru.sirius.group7.Gleb;

public class Game {
    public static void main(String[] args){
        Mob[] mob = new Mob[3];
        mob[0] = new Warrior(10);
        mob[1] = new Mage(5);
        mob[2] = new Bird(1);

        mob[0].move();
        ((Warrior)mob[0]).hit();

        for (int i = 0; i < mob.length; i++) {
            if(mob[i] instanceof Fly) {
                ((Fly) mob[i]).fly();
            }
        }


    }

}
