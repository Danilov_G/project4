package ru.sirius.group7.Gleb;

abstract class Mob implements Move{

    int hp;

    public Mob(int hp) {
        this.hp = hp;
    }
     abstract public void move();

}
