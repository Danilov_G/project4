package ru.sirius.group7.Gleb;

public class TaskSoldiers {
    public static void main(String[] args) {
        Soldier[] soldier = new Soldier[4];
        soldier[0] = new Soldier();
        soldier[1] = new Pilot();
        soldier[2] = new Submariner();
        soldier[3] = new Submariner();

        int count = 0;
        int countsub = 0;
        for(int i = 0; i < soldier.length; i++) {
            soldier[i].walk();
            count += soldier[i].a;
        }
        System.out.println(count);

        for(int j = 0; j < soldier.length; j++) {
            if (soldier[j] instanceof Submariner) {
                soldier[j].walk();
                countsub += soldier[j].a;
            }
        }
        countsub = countsub / 2;
        System.out.println(countsub);
    }
}
