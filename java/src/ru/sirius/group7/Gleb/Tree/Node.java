package ru.sirius.group7.Gleb.Tree;

public class Node {
    public Node parent;
    private Object data;

    Node (Node parent, Object data){
        this.parent = parent;
        this.data = data;
    }
    Node (){
    }
    public void setParent(Node parent){
        this.parent = parent;
    }
    public Node getParent(){
        return parent;
    }
    public void setData(Object data){
        this.data = data;
    }
    @Override
    public String toString(){
        if (data == null){
            return " ";
        }
        else {
            return this.data.toString();
        }
    }
}
